

#include <cmath>

#include "Charge.hh"

#include "TestSuite.hpp"

#include <cmath>

#include <iostream>

using namespace test;
using namespace ion_calculator;

using TestCase = TestSuite<Charge>::TestCase;

class OperatorEqualPlus : public TestCase
{
	int start_val_;

public:
	explicit OperatorEqualPlus(int start)
	    : TestCase("OperatorEqualPlus"), start_val_(start)
	{
	}
	~OperatorEqualPlus() {}

	bool exec(std::ostream &os, const Charge &charge) const override
	{
		auto copy(charge);
		copy += start_val_;
		return std::fabs(copy.inElectronCharge() -
				 2 * charge.inElectronCharge()) < 0.001;
	}
};

class OperatorPlus : public TestCase
{
	int start_val_;

public:
	explicit OperatorPlus(int start)
	    : TestCase("OperatorPlus"), start_val_(start)
	{
	}
	~OperatorPlus() {}

	bool exec(std::ostream &os, const Charge &charge) const override
	{
		return std::fabs((charge + 2).inElectronCharge() - start_val_ -
				 2) < 0.001;
	}
};

class InElectronCharge : public TestCase
{
	int start_val_;

public:
	explicit InElectronCharge(int start)
	    : TestCase("InElectronCharge"), start_val_(start)
	{
	}
	~InElectronCharge() {}

	bool exec(std::ostream &os, const Charge &charge) const override
	{
		return std::fabs(charge.inElectronCharge() - start_val_) <
		       0.001;
	}
};

class InColoumb : public TestCase
{
	int start_val_;

public:
	explicit InColoumb(int start) : TestCase("InColoumb"), start_val_(start)
	{
	}
	~InColoumb() {}

	bool exec(std::ostream &os, const Charge &charge) const override
	{
		return std::fabs(charge.inColoumb() - start_val_ * 1.602e-19) <
		       0.001;
	}
};

class OperatorSmallEqual : public TestCase
{
	int start_val_;

public:
	explicit OperatorSmallEqual(int start)
	    : TestCase("OperatorSmallEqual"), start_val_(start)
	{
	}
	~OperatorSmallEqual() {}

	bool exec(std::ostream &os, const Charge &charge) const override
	{
		auto equal = charge;
		if (!(equal <= charge)) {
			os << "\tcharge <= equal failed\n";
			return false;
		}
		auto smaller = Charge::fromElectronCharge(start_val_ - 5);
		if (!(smaller <= charge) && (charge <= smaller)) {
			os << "\tcharge <= smaller failed\n";
			return false;
		}
		return true;
	}
};

class OperatorGreater : public TestCase
{
	int start_val_;

public:
	explicit OperatorGreater(int start)
	    : TestCase("OperatorGreater"), start_val_(start)
	{
	}
	~OperatorGreater() {}

	bool exec(std::ostream &os, const Charge &charge) const override
	{
		auto smaller = Charge::fromElectronCharge(start_val_ - 5);
		return ((charge > smaller) && !(smaller > charge));
	}
};

int main()
{
	int val = (double)rand() / 2;
	Charge c = Charge::fromElectronCharge(val);

	TestSuite<Charge> suite(std::move(c));

	suite.add(new OperatorEqualPlus(val));
	suite.add(new OperatorPlus(val));
	suite.add(new InElectronCharge(val));
	suite.add(new InColoumb(val));
	suite.add(new OperatorSmallEqual(val));
	suite.add(new OperatorGreater(val));

	return suite.exec(std::cout);
}
