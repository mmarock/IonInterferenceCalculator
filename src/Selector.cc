

#include "Selector.hh"

#include <algorithm>

#include <cmath>

#include "Element.hh"
#include "Logger.hh"

namespace ion_calculator
{
Selector::Selector(const MagneticField &field, const Mass &mass_deviation,
		   const Length &radius, const Potential &terminal,
		   const Potential &injection, const Charge &max)
    : mtable_(field, radius, terminal, max), deviation_(mass_deviation),
      injection_(injection)
{
}

const MagneticField &Selector::magneticField() const noexcept
{
	return mtable_.magneticField();
}

const Mass &Selector::massDeviation() const noexcept { return deviation_; }

const Length &Selector::radius() const noexcept { return mtable_.radius(); }

const Potential &Selector::terminalVoltage() const noexcept
{
	return mtable_.terminalVoltage();
}

const Charge &Selector::maxCharge() const noexcept
{
	return mtable_.maxCharge();
}

void Selector::set(const MagneticField &field) { mtable_.set(field); }

void Selector::set(const Mass &mass_deviation) { deviation_ = mass_deviation; }

void Selector::set(const Length &radius) { mtable_.set(radius); }

void Selector::set(const Potential &terminal) { mtable_.set(terminal); }

void Selector::set(const Charge &max) { mtable_.set(max); }

const MassTable &Selector::massTable() const noexcept { return mtable_; }

void Selector::query(const Element &element, ResultVector &res) const noexcept
{
	// for each charge
	// 	if charge > atom_number
	// 		break;
	//
	// 	if mass smaller mtable-dev
	// 		continue
	// 	if mass greater mtable+dev
	// 		break

	const auto &terminal = mtable_.terminalVoltage();
	const auto &injection_energy =
	    Charge::fromElectronCharge(1) * injection_;
	for (const auto &mtc : mtable_.data()) {
		if (mtc.charge.inElectronCharge() > element.atomNumber()) {
			return;
		}
		if (element.mass() < mtc.mass - deviation_) {
			return;
		}
		if (element.mass() > mtc.mass + deviation_) {
			continue;
		}

		double overlap = element.mass() / mtc.mass;

		res.push_back(
		    { element, mtc.charge, overlap,
		      (mtc.charge + 1) * terminal + injection_energy });
	}
}

} // namespace ion_calculator
