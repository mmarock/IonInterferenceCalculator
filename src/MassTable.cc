

#include "MassTable.hh"

#include <cmath>

#include <cassert>

namespace ion_calculator
{

MassTable::MassTable(const MagneticField &field, const Length &radius,
		     const Potential &terminal_voltage, const Charge &max)
    : mass_table_(init_table(field, radius, terminal_voltage, max)),
      field_(field), radius_(radius), terminal_voltage_(terminal_voltage),
      max_(max)
{
}

const MagneticField &MassTable::magneticField() const noexcept
{
	return field_;
}

const Length &MassTable::radius() const noexcept { return radius_; }

const Potential &MassTable::terminalVoltage() const noexcept
{
	return terminal_voltage_;
}

const Charge &MassTable::maxCharge() const noexcept { return max_; }

void MassTable::set(const MagneticField &field)
{
	field_ = field;
	recalc();
}

void MassTable::set(const Length &radius)
{
	radius_ = radius;
	recalc();
}

void MassTable::set(const Potential &terminal)
{
	terminal_voltage_ = terminal;
	recalc();
}

void MassTable::set(const Charge &max)
{
	max_ = max;
	recalc();
}

const std::vector<MassTable::Data> &MassTable::data() const noexcept
{
	return mass_table_;
}

void MassTable::recalc()
{
	mass_table_ = init_table(field_, radius_, terminal_voltage_, max_);
}

std::vector<MassTable::Data>
MassTable::init_table(const MagneticField &field, const Length &radius,
		      const Potential &terminal_voltage, const Charge &max)
{
	std::vector<Data> rc;
	rc.reserve(max.inElectronCharge());

	Charge charge = Charge::fromElectronCharge(1);
	Mass old_mass;
	Mass new_mass;
	do {
		old_mass = new_mass;

		new_mass =
		    Mass::fromKg(std::pow(field.inTesla() * radius.inMeter() *
					      charge.inColoumb(),
					  2.0) /
				 (2.0 * terminal_voltage.inVolt() *
				  (charge + 1).inColoumb())) +
		    Mass::fromMevCSquared(0.511 * charge.inElectronCharge());

		rc.push_back({ charge, new_mass });

		charge += 1;
	} while (charge <= max);
	return rc;
}

} // namespace ion_calculator
