

#include "Element.hh"

#include <algorithm>

#include <cassert>

#include "Logger.hh"

namespace ion_calculator
{

Element::Element(unsigned int atom_number, const std::string &symbol,
		 double isotopic_composition, unsigned int neutron_number,
		 const Mass &mass)
    : atom_number_(atom_number), neutron_number_(neutron_number),
      isotopic_composition_(isotopic_composition), atomic_mass_(mass), symbol_(symbol)
{

}

unsigned int Element::atomNumber() const noexcept { return atom_number_; }

unsigned int Element::neutronNumber() const noexcept { return neutron_number_; }

const double &Element::isotopicComposition() const noexcept
{
	return isotopic_composition_;
}

const Mass &Element::mass() const noexcept { return atomic_mass_; }

const char *Element::symbol() const noexcept
{
	return symbol_.c_str();
}



} // namespace ion_calculator
