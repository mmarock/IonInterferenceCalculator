

#include "Database.hh"

#include <cassert>
#include <cstdlib>

#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include "Element.hh"
#include "Logger.hh"

namespace logger
{

enum FUNCTION : uint8_t {
	BY_FILE,
	BY_NETWORK,
	CONNECT,
	HANDSHAKE,
	WRITE,
	READ,
	READ_UNTIL
};

static const char *function_str(enum FUNCTION function)
{
	switch (function) {
	case BY_FILE:
		return "by_file()";
	case BY_NETWORK:
		return "by_network()";
	case CONNECT:
		return "connect()";
	case HANDSHAKE:
		return "handshake()";
	case WRITE:
		return "write()";
	case READ:
		return "read()";
	case READ_UNTIL:
		return "read_until()";
	};
}

static void debug(enum FUNCTION function, const char *dbg)
{
	log(SE_DEBUG, "Database: %s: %s", function_str(function), dbg);
}

} // namespace logger

namespace ion_calculator
{

DatabaseException::DatabaseException(const char *msg)
    : msg_(std::string("Database: ") + msg)
{
}
DatabaseException::DatabaseException(const std::string &msg)
    : msg_("Database: " + msg)
{
}

const char *DatabaseException::what() const noexcept { return msg_.c_str(); }

class BoostErrorException : public DatabaseException
{
public:
	enum Function : uint8_t {
		CONNECT,
		HANDSHAKE,
		READ,
		VERIFY_HEADER,
		WRITE,
		READ_UNTIL
	};

	BoostErrorException(const Function &f,
			    const boost::system::error_code &code)
	    : DatabaseException(str(f) + ": " + code.message())
	{
	}

private:
	static std::string str(const Function &f)
	{
		switch (f) {
		case CONNECT:
			return "connect()";
		case HANDSHAKE:
			return "handshake()";
		case READ:
			return "read()";
		case VERIFY_HEADER:
			return "verify_header()";
		case WRITE:
			return "write()";
		case READ_UNTIL:
			return "read_until()";
		};
	}
};

class DatabaseEmpty : public DatabaseException {
public:
	explicit DatabaseEmpty() : DatabaseException("is empty") {}
};

////////////////////////////////////////////////////////////////////////////////
//		Database::Impl definition
////////////////////////////////////////////////////////////////////////////////
class Database::Impl
{
	std::string path_;
	std::vector<Element> elements_;

public:
	// with default path
	explicit Impl(const std::string &path);
	~Impl();

	Impl(const Database &rhs) = delete;
	Impl &operator=(const Impl &rhs) = delete;

	const std::string &path() const noexcept;
	const std::vector<Element> &elements() const noexcept;
	const Element &greatestElement() const;

private:
	static boost::asio::io_service &get_io_service();
	static boost::asio::ssl::context &get_ssl_context();

	static std::vector<Element> import(const std::string &path);

	static constexpr char *host() noexcept;

	static void compile_request(const std::string &host,
				    boost::asio::streambuf &buf);

	static int parse_header(boost::asio::streambuf &buf);

	static std::vector<Element> by_network();
	static std::vector<Element> by_file(const std::string &path);

	static int to_file(const std::string &path,
			   const std::vector<Element> &elements) noexcept;

	static int consume_atom_number(std::istream &is, std::string &line,
				       unsigned int &atom_number);
	static int consume_atom_symbol(std::istream &is, std::string &line,
				       std::string &atom_symbol);
	static int consume_mass_number(std::istream &is, std::string &line,
				       unsigned int &mass_number);
	static int consume_atom_mass(std::istream &is, std::string &line,
				     Mass &atom_mass);
	static int consume_isotopic_composition(std::istream &is,
						std::string &line,
						double &isotopic_comp);

}; // class Database

Database::Impl::Impl(const std::string &path)
    : path_(path), elements_(import(path))
{
}

Database::Impl::~Impl() {}

const std::string &Database::Impl::path() const noexcept { return path_; }

const std::vector<Element> &Database::Impl::elements() const noexcept
{
	return elements_;
}

const Element &Database::Impl::greatestElement() const
{
	auto it =
	    std::max_element(elements_.begin(), elements_.end(),
			     [](const Element &lhs, const Element &rhs) {
				     return lhs.atomNumber() < rhs.atomNumber();
			     });

	if (it == elements_.end()) {
		throw DatabaseEmpty();
	}
	return *it;
}

boost::asio::io_service &Database::Impl::get_io_service()
{
	static boost::asio::io_service service;
	return service;
}

boost::asio::ssl::context &Database::Impl::get_ssl_context()
{

	using namespace boost::asio::ssl;
	using boost::asio::ssl::context;
	static context ctx(context::sslv23);
	static bool initialised(false);
	if (!initialised) {
		ctx.set_verify_mode(verify_peer);
		ctx.set_default_verify_paths();
		initialised = true;
	}
	return ctx;
}

std::vector<Element> Database::Impl::import(const std::string &path)
{
	// try file load
	try {
		return by_file(path);
	} catch (const std::runtime_error &ex) {
		logger::debug(logger::BY_FILE, ex.what());
	}
	try {
		auto elements = by_network();
		to_file(path, elements);
		return elements;
	} catch (const std::runtime_error &ex) {
		throw DatabaseException(ex.what());
	}
}

void Database::Impl::compile_request(const std::string &host,
				     boost::asio::streambuf &buf)
{
	std::ostream stream(&buf);
	stream << "GET "
		  "/cgi-bin/Compositions/"
		  //"stand_alone.pl?ele=C&ascii=ascii2&isotype=all "
		  "stand_alone.pl?ele=&all=all&ascii=ascii2&isotype=all "
		  "HTTP/1.1\r\n";
	stream << "Host: " << host << "\r\n";
	stream << "Accept: text/html\r\n";
	stream << "Connection: close\r\n\r\n";
}

int Database::Impl::parse_header(boost::asio::streambuf &buf)
{
	buf.consume(buf.size() + 1);
	return 0;
}

std::vector<Element> Database::Impl::by_network()
{
	using boost::asio::ip::tcp;
	using namespace boost::asio;
	using ssl_socket = ssl::stream<ip::tcp::socket>;

	logger::debug(logger::BY_NETWORK, "load");

	std::vector<Element> elements;

	// get service object
	auto &io = get_io_service();

	const std::string host("physics.nist.gov");

	// resolve server
	tcp::resolver resolver(io);
	tcp::resolver::query query(host, "https");

	boost::system::error_code err;

	// connect to server
	ssl_socket socket(io, get_ssl_context());
	connect(socket.lowest_layer(), resolver.resolve(query), err);
	if (err) {
		throw BoostErrorException(BoostErrorException::CONNECT, err);
	}
	logger::debug(logger::CONNECT, host.c_str());

	// set the needed verification and do the handshake
	socket.set_verify_mode(boost::asio::ssl::verify_peer);
	socket.set_verify_callback(ssl::rfc2818_verification(host));

	socket.handshake(ssl_socket::client, err);
	if (err) {
		throw BoostErrorException(BoostErrorException::HANDSHAKE, err);
	}
	logger::debug(logger::HANDSHAKE, "finished");

	// compile and send the request
	boost::asio::streambuf streambuf;
	compile_request(host, streambuf);
	// write ALL the bytes, or error
	boost::asio::write(socket, streambuf, err);
	if (err) {
		throw BoostErrorException(BoostErrorException::WRITE, err);
	}
	logger::debug(logger::WRITE, "sent request");

	// consume entire buffer, then read header
	streambuf.consume(streambuf.size() + 1);
	boost::asio::read_until(socket, streambuf, "\r\n", err);
	if (err) {
		throw BoostErrorException(BoostErrorException::READ_UNTIL, err);
	}
	logger::debug(logger::READ_UNTIL, "received header");

	if (parse_header(streambuf)) {
		throw BoostErrorException(BoostErrorException::VERIFY_HEADER,
					  err);
	}

	// next line seems to be content size
	boost::asio::read_until(socket, streambuf, "\r\n\r\n", err);
	// eof indicates "connection closed by peer"
	// ssl::stream_truncated is basically a "short read" error
	// both are only distiguishable by the category...
	// IF there is an eof AND the category is ssl, this is no real error...
	if (err && err != boost::asio::error::eof &&
	    err.category() == boost::asio::error::get_ssl_category()) {
		throw BoostErrorException(BoostErrorException::READ, err);
	}

	std::istream is(&streambuf);
	std::string line;
	while (std::getline(is, line)) {
		unsigned int atom_number(0);
		std::string atom_symbol;
		unsigned int mass_number(0);
		Mass atom_mass;
		double isotopic_comp(0);
		// TODO ATOM-> ATOMIC !!
		if (consume_atom_number(is, line, atom_number) ||
		    consume_atom_symbol(is, line, atom_symbol) ||
		    consume_mass_number(is, line, mass_number) ||
		    consume_atom_mass(is, line, atom_mass) ||
		    consume_isotopic_composition(is, line, isotopic_comp)) {
			// logger::log(logger::SE_DEBUG, "...failed");
			continue;
		}
		logger::log(logger::SE_DEBUG,
			    "Database: add Element Z=%d,A=%d,%s", atom_number,
			    mass_number, atom_symbol.c_str());
		elements.emplace_back(atom_number, atom_symbol, isotopic_comp,
				      mass_number - atom_number, atom_mass);
	}

	return elements;
}

std::vector<Element> Database::Impl::by_file(const std::string &path_str)
{
	using namespace boost::filesystem;
	using boost::filesystem::ifstream;
	using boost::property_tree::ptree;

	logger::log(logger::SE_DEBUG, "Database: load file:%s",
		    path_str.c_str());
	std::vector<Element> elements;
	ifstream ifs(path_str);

	ptree data;
	read_xml(ifs, data);
	for (auto &enode : data.get_child("elements")) {
		if (enode.first != "element") {
			logger::log(logger::SE_NOTICE,
				    "Database: invalid subnode elements::%s",
				    enode.first.c_str());
			continue;
		}

		elements.emplace_back(
		    enode.second.get<unsigned int>("atom_number"),
		    enode.second.get<std::string>("symbol"),
		    enode.second.get<double>("isotopic_composition"),
		    enode.second.get<unsigned int>("neutron_number"),
		    Mass::fromAmu(enode.second.get<double>("atomic_mass")));
	}
	return elements;
}

/**
 * @brief
 *
 * @todo error management
 *
 * @param path
 * @param elements
 *
 * @return
 */
int Database::Impl::to_file(const std::string &path,
			    const std::vector<Element> &elements) noexcept
{
	using namespace boost::filesystem;
	using boost::filesystem::ofstream;
	using boost::property_tree::ptree;

	logger::log(logger::SE_DEBUG, "Database: save file:%s", path.c_str());

	ptree root;
	// elements subnode, now populate
	ptree elements_node;
	for (const Element &e : elements) {

		ptree element_node;

		element_node.put<unsigned int>("atom_number", e.atomNumber());
		element_node.put<const char *>("symbol", e.symbol());
		element_node.put<unsigned int>("neutron_number",
					       e.neutronNumber());
		element_node.put<double>("atomic_mass", e.mass().inAmu());
		element_node.put<double>("isotopic_composition",
					 e.isotopicComposition());

		elements_node.add_child("element", element_node);
	}

	// elements node
	root.add_child("elements", elements_node);

	ofstream os(path);

	write_xml(os, root);

	return 0;
}
int Database::Impl::consume_atom_number(std::istream &is, std::string &line,
					unsigned int &atom_number)
{
	if (line.find("Atomic Number") == std::string::npos) {
		return -1;
	}
	auto c = line.find("=");
	if (c == std::string::npos) {
		return -1;
	}
	char *end = nullptr;
	atom_number = strtoul(line.c_str() + c + 1, &end, 10);
	logger::log(logger::SE_DEBUG, "atom_number = %d", atom_number);
	if (atom_number == 0 && errno == ERANGE) {
		return -1;
	} else {
		std::getline(is, line);
		return 0;
	}
}

int Database::Impl::consume_atom_symbol(std::istream &is, std::string &line,
					std::string &atom_symbol)
{
	if (line.find("Atomic Symbol") == std::string::npos) {
		return -1;
	}
	auto c = line.find("=");
	if (c == std::string::npos) {
		return -1;
	}
	atom_symbol = line.substr(c + 2, line.size() - c - 2);
	logger::log(logger::SE_DEBUG, "atom_symbol = %s", atom_symbol.c_str());
	std::getline(is, line);
	return 0;
}
int Database::Impl::consume_mass_number(std::istream &is, std::string &line,
					unsigned int &mass_number)
{
	if (line.find("Mass Number") == std::string::npos) {
		return -1;
	}
	auto c = line.find("=");
	if (c == std::string::npos) {
		return -1;
	}
	char *end = nullptr;
	mass_number = strtoul(line.c_str() + 1 + c, &end, 10);
	logger::log(logger::SE_DEBUG, "mass_number = %d", mass_number);
	if (mass_number == 0 && errno == ERANGE) {
		return -1;
	} else {
		std::getline(is, line);
		return 0;
	}
}
int Database::Impl::consume_atom_mass(std::istream &is, std::string &line,
				      Mass &atom_mass)
{
	if (line.find("Relative Atomic Mass") == std::string::npos) {
		return -1;
	}
	auto c = line.find("=");
	if (c == std::string::npos) {
		return -1;
	}
	char *end = nullptr;
	double mass(0);
	mass = strtold(line.c_str() + c + 1, &end);
	if (errno == ERANGE) {
		return -1;
	} else {
		atom_mass = Mass::fromAmu(mass);
		logger::log(logger::SE_DEBUG, "atom_mass = %lf",
			    atom_mass.inAmu());
		std::getline(is, line);
		return 0;
	}
}

int Database::Impl::consume_isotopic_composition(std::istream &is,
						 std::string &line,
						 double &isotope_comp)
{
	if (line.find("Isotopic Composition") == std::string::npos) {
		return -1;
	}
	auto c = line.find("=") + 1;
	if (c == std::string::npos) {
		return -1;
	}
	char *end = nullptr;
	isotope_comp = strtold(line.c_str() + 1 + c, &end);
	logger::log(logger::SE_DEBUG, "isotopic composition = %lf",
		    isotope_comp);
	if (errno == ERANGE || end == line.c_str() + 1 + c) {
		logger::log(logger::SE_DEBUG,
			    "isotopic composition not existing, ignore");
		return -1;
	}
	return 0;
}

////////////////////////////////////////////////////////////
//		Database definition
////////////////////////////////////////////////////////////

Database::Database(const std::string &path) : impl_(new Impl(path)) {}

Database::~Database() {}

const std::string &Database::path() const noexcept
{
	assert(impl_ && "impl is NULL!");
	return impl_->path();
}

const std::vector<Element> &Database::elements() const noexcept
{
	assert(impl_ && "impl is NULL!");
	return impl_->elements();
}

const Element &Database::greatestElement() const
{
	assert(impl_ && "impl is NULL!");
	return impl_->greatestElement();
}

} // namespace ion_calculator
