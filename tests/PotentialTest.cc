

#include "TestSuite.hpp"

#include "Potential.hh"

#include <iostream>

#include <cmath>

using namespace test;
using namespace ion_calculator;
using TestCase = TestSuite<Potential>::TestCase;

class InKVolt : public TestCase
{
	double start_value_;

public:
	InKVolt(double start_value)
	    : TestCase("InKVolt"), start_value_(start_value)
	{
	}
	~InKVolt() {}

	bool exec(std::ostream &os, const Potential &p) const override
	{
		(void)os;
		return std::fabs(p.inKVolt() - start_value_) < 0.001;
	}
};

class InVolt : public TestCase
{
	double start_value_;

public:
	InVolt(double start_value)
	    : TestCase("InVolt"), start_value_(start_value)
	{
	}
	~InVolt() {}

	bool exec(std::ostream &os, const Potential &p) const override
	{
		(void)os;
		return std::fabs(p.inVolt() - start_value_ * 1000) < 0.001;
	}
};

int main()
{

	double val = (double)rand() / RAND_MAX * 500;
	Potential p = Potential::fromKVolt(val);

	TestSuite<Potential> suite(std::move(p));

	suite.add(new InKVolt(val));
	suite.add(new InVolt(val));

	return suite.exec(std::cout);
}
