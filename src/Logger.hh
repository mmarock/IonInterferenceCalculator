

#ifndef LOGGER_HH_INCLUDED
#define LOGGER_HH_INCLUDED

#include <cstdint>

namespace logger
{

enum SEVERITY : uint8_t { SE_DEBUG, SE_NOTICE, SE_INFO, SE_ERROR };

void log(enum SEVERITY severity, const char *fmt, ...);

} // namespace logger

#endif // LOGGER_HH_INCLUDED
