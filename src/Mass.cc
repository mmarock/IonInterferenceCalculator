

#include "Mass.hh"

namespace ion_calculator
{

////////////////////////////////////////////////////////////
//		Mass definitions
////////////////////////////////////////////////////////////

Mass::Mass(const double &value) : amu_(value) {}

Mass::Mass() : amu_(0) {}

Mass Mass::fromKg(const double &kg) { return Mass(kg / Mass::amu()); }

Mass Mass::fromAmu(const double &amu) { return Mass(amu); }

Mass Mass::fromMevCSquared(const double &mev_c_squared)
{
	return Mass(mev_c_squared * Mass::mev_c_squared());
}

double Mass::inKg() const noexcept { return amu_ * amu(); }

const double &Mass::inAmu() const noexcept { return amu_; }

double Mass::inMevCSquared() const noexcept { return amu_ / mev_c_squared(); }

constexpr double Mass::amu() noexcept { return 1.660539e-27; }

constexpr double Mass::mev_c_squared() noexcept { return 0.00107354411; }

////////////////////////////////////////////////////////////
//		non member functions
////////////////////////////////////////////////////////////

double operator/(const Mass &lhs, const Mass &rhs)
{
	return lhs.amu_  / rhs.amu_;
}

Mass operator-(const Mass &lhs, const Mass &rhs)
{
	return Mass::fromAmu(lhs.amu_ - rhs.amu_);
}

Mass operator+(const Mass &lhs, const Mass &rhs)
{
	return Mass::fromAmu(lhs.amu_ + rhs.amu_);
}

bool operator<(const Mass &lhs, const Mass &rhs)
{
	return lhs.amu_ < rhs.amu_;
}

bool operator>(const Mass &lhs, const Mass &rhs)
{
	return lhs.amu_ > rhs.amu_;
}

} // namespace ion_calculator
