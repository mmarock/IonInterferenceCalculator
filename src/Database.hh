

#ifndef DATABASE_HH_INCLUDED
#define DATABASE_HH_INCLUDED

#include <memory>
#include <string>
#include <vector>

namespace ion_calculator
{

class Element;

class DatabaseException : public std::exception {
	std::string msg_;
public:
	explicit DatabaseException(const char *what);
	explicit DatabaseException(const std::string &what);

	const char *what() const noexcept override;
};

class Database
{
	class Impl;
	std::unique_ptr<Impl> impl_;

public:
	explicit Database(const std::string &path);
	~Database();

	Database(const Database &rhs) = delete;
	Database &operator=(const Database &rhs) = delete;

	const std::string &path() const noexcept;
	const std::vector<Element> &elements() const noexcept;
	const Element &greatestElement() const;

}; // class Database

} // ion_calculator

#endif // DATABASE_HH_INCLUDED
