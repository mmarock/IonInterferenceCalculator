

#include "Logger.hh"

#include <string>

#include <syslog.h>

#include <cstdarg>
#include <cassert>

namespace logger {

static int severity(enum SEVERITY s) {
	switch(s) {
		case SE_DEBUG:
			return LOG_DEBUG;
		case SE_NOTICE:
			return LOG_NOTICE;
		case SE_INFO:
			return LOG_INFO;
		case SE_ERROR:
			return LOG_ERR;
	}
}

struct LogObject {
	explicit LogObject() {
		openlog("IIC", LOG_PID | LOG_CONS , LOG_USER);
		syslog(LOG_DEBUG, "opened");
	}

	~LogObject() {
		syslog(LOG_DEBUG, "...closed");
		closelog();
	}

};

void log(enum SEVERITY sev, const char *fmt, ...) {
	assert(fmt && "fmt is NULL");

	static LogObject obj;

	va_list va;
	va_start(va, fmt);
	vsyslog(severity(sev),fmt, va);
	va_end(va);
}


} // namespace logger
