

#include "Energy.hh"
#include "Charge.hh"
#include "Potential.hh"

namespace ion_calculator
{

Energy::Energy() {}

Energy::Energy(const double &ev) : val_in_ev_(ev) {}

Energy Energy::fromMeV(const double &mev) { return Energy(mev * 1e6); }

Energy Energy::fromEv(const double &ev) { return Energy(ev); }

double Energy::inMev() const noexcept {
	return val_in_ev_ / 1e6;
}

Energy operator+(const Energy &lhs, const Energy &rhs) {
	return Energy(lhs.val_in_ev_ + rhs.val_in_ev_);
}

Energy operator*(const Charge &charge, const Potential &potential)
{
	return Energy::fromEv(charge.inElectronCharge() * potential.inVolt());
}

} // namespace ion_calculator
