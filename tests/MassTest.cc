

#include "Mass.hh"

#include "TestSuite.hpp"

#include <iostream>

#include <cmath>

using namespace test;
using namespace ion_calculator;
using TestCase = TestSuite<Mass>::TestCase;

class FromKg : public TestCase
{
	double init_val_;

public:
	FromKg(const double &val) : TestCase("FromKg"), init_val_(val) {}
	~FromKg() {}

	bool exec(std::ostream &os, const Mass &mass) const override
	{
		auto from_kg = Mass::fromKg(1.660539e-27 * init_val_);
		return std::fabs((mass - from_kg).inAmu()) < 0.0001;
	}
};

class FromMevCSquared : public TestCase
{
	double init_val_;

public:
	FromMevCSquared(const double &val)
	    : TestCase("FromMevCSquared"), init_val_(val)
	{
	}
	~FromMevCSquared() {}
	bool exec(std::ostream &os, const Mass &mass) const override
	{
		auto mev = Mass::fromMevCSquared(init_val_ * 931.494095757276);
		return std::fabs((mev - mass).inAmu()) < 0.0001;
	}
};

class InKg : public TestCase
{
	double init_val_;

public:
	InKg(const double &val) : TestCase("InKg"), init_val_(val) {}
	~InKg() {}
	bool exec(std::ostream &os, const Mass &mass) const override
	{
		return std::fabs(init_val_ * 1.660539040e-27 - mass.inKg()) <
		       0.0001;
	}
};

class InAmu : public TestCase
{
	double init_val_;

public:
	InAmu(const double &val) : TestCase("InAmu"), init_val_(val) {}
	~InAmu() {}

	bool exec(std::ostream &os, const Mass &mass) const override
	{
		return std::fabs(mass.inAmu() - init_val_) < 0.0001;
	}
};

class InMevCSquared : public TestCase
{
	double init_val_;

public:
	InMevCSquared(const double &val)
	    : TestCase("InMevCSquared"), init_val_(val)
	{
	}
	~InMevCSquared() {}
	bool exec(std::ostream &os, const Mass &mass) const override
	{
		return std::fabs((mass.inMevCSquared() -
				  init_val_ * 931.4940957572763)) < 0.0001;
	}
};

class OperatorDiv : public TestCase
{
	double init_val_;

public:
	OperatorDiv(const double &val) : TestCase("OperatorDiv"), init_val_(val)
	{
	}
	~OperatorDiv() {}

	bool exec(std::ostream &os, const Mass &mass) const override
	{
		auto cmp = Mass::fromAmu(init_val_);
		return std::fabs(mass / cmp - 1l) < 0.0001;
	}
};

class OperatorMinus : public TestCase
{
	double init_val_;

public:
	OperatorMinus(const double &val)
	    : TestCase("OperatorMinus"), init_val_(val)
	{
	}
	~OperatorMinus() {}
	bool exec(std::ostream &os, const Mass &mass) const override
	{
		(void)init_val_;
		Mass copy = mass;
		return std::fabs((mass - copy).inAmu()) < 0.0001;
	}
};

class OperatorPlus : public TestCase
{

public:
	explicit OperatorPlus() : TestCase("OperatorPlus") {}
	~OperatorPlus() {}
	bool exec(std::ostream &os, const Mass &mass) const override
	{
		auto cpy = mass + mass;
		return std::fabs(cpy.inAmu() / 2 - mass.inAmu()) < 0.0001;
	}
};

class OperatorSmaller : public TestCase
{
public:
	explicit OperatorSmaller() : TestCase("OperatorSmaller") {}
	~OperatorSmaller() {}
	bool exec(std::ostream &os, const Mass &mass) const override
	{
		auto cpy = mass + mass;
		return mass < cpy && !(cpy < mass);
	}
};

class OperatorGreater : public TestCase
{
public:
	explicit OperatorGreater() : TestCase("OperatorGreater") {}
	~OperatorGreater() {}
	bool exec(std::ostream &os, const Mass &mass) const override
	{
		auto cpy = mass + mass;
		return cpy > mass && !(mass > cpy);
	}
};

int main()
{

	double val = (double)rand() / 2;
	Mass m = Mass::fromAmu(val);
	(void)m;

	TestSuite<Mass> suite(std::move(m));

	suite.add(new FromKg(val));
	suite.add(new FromMevCSquared(val));
	suite.add(new InKg(val));
	suite.add(new InAmu(val));
	suite.add(new InMevCSquared(val));
	suite.add(new OperatorDiv(val));
	suite.add(new OperatorMinus(val));
	suite.add(new OperatorPlus());
	suite.add(new OperatorSmaller());
	suite.add(new OperatorGreater());

	return suite.exec(std::cout);
}
