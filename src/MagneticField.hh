

#ifndef MAGNETICFIELD_HH_INCLUDED
#define MAGNETICFIELD_HH_INCLUDED

namespace ion_calculator {

class MagneticField {
	double field_in_tesla_;

	MagneticField(const double &in_tesla_);
public:
	static MagneticField fromTesla(const double &tesla) noexcept;
	static MagneticField fromGauss(const double &gauss) noexcept;

	const double &inTesla() const noexcept;
	double inGauss() const noexcept;

private:
	static constexpr double tesla_to_gauss() noexcept;
};

} // namespace ion_calculator

#endif // MAGNETICFIELD_HH_INCLUDED
