

#ifndef TESTSUITE_HH_INCLUDED
#define TESTSUITE_HH_INCLUDED

#include <algorithm>
#include <iomanip>
#include <memory>
#include <ostream>
#include <vector>

#include <cassert>

namespace test
{

/*class TestSuiteException : public std::exception
{
	std::string msg_;

public:
	TestSuiteException(const std::string &msg)
	    : msg_(std::string("TestSuiteException: ") + msg)
	{
	}
	TestSuiteException(const char *msg)
	    : msg_(std::string("TestSuiteException: ") + msg)
	{
	}

	const char *what() const noexcept { return msg_.c_str(); }
};*/

template <class TestableObject>
class TestSuite
{
public:
	class TestCase
	{
		std::string name_;

	public:
		using Ptr = TestCase *;

		explicit TestCase(const char *name) : name_(name) {}
		virtual ~TestCase() {}

		const std::string &name() const noexcept { return name_; }

		virtual bool exec(std::ostream &os,
				  const TestableObject &obj) const
		{
			assert(false && "impl that shit in your derived class");
			return false;
		}

	}; // struct TestCase

private:
	TestableObject obj_;

	std::vector<typename TestCase::Ptr> cases_;

public:
	TestSuite(TestableObject &&obj)
	    : obj_(std::forward<TestableObject>(obj))
	{
	}

	~TestSuite()
	{
		for (auto ptr : cases_) {
			delete ptr;
		}
	}

	void add(typename TestCase::Ptr testptr) {
		assert(testptr && "testptr is NULL");
		cases_.push_back(testptr); }

	int exec(std::ostream &os) const
	{
		auto failed =
		    std::find_if(cases_.begin(), cases_.end(),
				 [&](const typename TestCase::Ptr c) {
					 os << std::left << std::setw(15)
					    << std::string("[ TestCase ] ")
					    << std::setw(20) << c->name()
					    << " exec\n"
					    << std::internal;
					 return !c->exec(os, obj_);
				 });
		if (failed != cases_.end()) {
			os << std::left << std::setw(15)
			   << std::string("[ Error ] ") << std::setw(20)
			   << (*failed)->name() << " failed\n";
			return -1;
		}
		return 0;
	}

}; // class TestSuite

} // namespace test

#endif // TESTSUITE_HH_INCLUDED
