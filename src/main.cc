
#include <cmath>

#include <algorithm>

#include "Database.hh"
#include "Element.hh"
#include "Length.hh"
#include "MagneticField.hh"
#include "MassTable.hh"
#include "Output.hh"
#include "Potential.hh"
#include "Selector.hh"

#include <iostream>

#include <unistd.h>

using namespace ion_calculator;

int main(int argc, char **argv)
{
	// boost::program_options::parse_command_line(argc, argv);
	// help
	// --field T|Gauss
	// --terminal V|kV
	// --mass_deviation amu
	// --max-atom-number=30

	auto terminal = Potential::fromKVolt(561);
	auto deviation = Mass::fromMevCSquared(200);
	auto radius = Length::fromMeter(0.971277);
	auto injection = Potential::fromKVolt(78);
	auto field = MagneticField::fromGauss(3113.9);

	Output out(std::cout);
	try {

		Database db("db.xml"); // throw if failed
		out.print(db);

		Selector s(field, deviation, radius, terminal, injection,
			   Charge::fromElectronCharge(
			       db.greatestElement().atomNumber()));

		Selector::ResultVector result;
		out.print(s);

		for (const auto &e : db.elements()) {
			s.query(e, result);
		}

		std::sort(
		    result.begin(), result.end(),
		    [](const IonizedElement &lhs, const IonizedElement &rhs) {
			    return lhs.mass_overlap < rhs.mass_overlap;
		    });

		for (const auto &r : result) {
			out.print(r);
		}

	} catch (const DatabaseException &ex) {
		out.error(ex);
	} catch (const std::exception &ex) {
		out.error(ex);
	}

	return 0;
}
