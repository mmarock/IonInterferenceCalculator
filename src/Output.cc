

#include "Output.hh"

#include "Database.hh"
#include "Element.hh"
#include "MassTable.hh"
#include "Selector.hh"

#include <iomanip>
#include <ostream>

namespace ion_calculator
{

static std::ostream &put_element(std::ostream &os, const Element &element)
{
	os << std::setw(5) << element.symbol() << " Z:" << std::setw(5)
	   << element.atomNumber() << " N:" << std::setw(5)
	   << element.neutronNumber()
	   << " IsotopicComposition:" << std::setw(10)
	   << element.isotopicComposition() << std::setw(5)
	   << " mass:" << element.mass().inAmu();
	return os;
}

////////////////////////////////////////////////////////////
//		Output definitions
////////////////////////////////////////////////////////////

Output::Output(std::ostream &os) : os_(os) {}

void Output::print(const Database &value)
{
	os_ << std::setw(20) << "[ Database ] I have " << std::setw(4)
	    << value.elements().size() << " imported elements, located in "
	    << value.path() << std::endl;
}

void Output::print(const Element &value)
{
	put_element(os_, value) << std::endl;
}

void Output::print(const IonizedElement &value)
{
	os_ << std::setw(20) << "[ Selector ] charge:" << std::setw(3)
	    << value.charge.inElectronCharge()
	    << "e mass_overlap:" << std::setw(10) << value.mass_overlap
	    << " energy:" << std::setw(10) << value.energy.inMev() << "MeV ";
	put_element(os_, value.element) << std::endl;
}

void Output::print(const Selector &value)
{
	os_ << std::setw(20) << "[ Selector ] initialized with field:"
	    << value.magneticField().inTesla() << std::setw(5)
	    << "T radius:" << value.radius().inMeter() << std::setw(5)
	    << "m terminal:" << value.terminalVoltage().inKVolt()
	    << std::setw(5)
	    << "kV max_charge:" << value.maxCharge().inElectronCharge() << "e"
	    << std::endl;
}

void Output::error(const std::exception &value)
{
	os_ << "[ error ] " << value.what() << std::endl;
}

} // namespace ion_calculator
