

#include "MagneticField.hh"

#include <cassert>

namespace ion_calculator
{

MagneticField::MagneticField(const double &tesla) : field_in_tesla_(tesla) {}

MagneticField MagneticField::fromTesla(const double &tesla) noexcept
{
	return MagneticField(tesla);
}

MagneticField MagneticField::fromGauss(const double &gauss) noexcept
{
	return MagneticField(gauss / MagneticField::tesla_to_gauss());
}

const double &MagneticField::inTesla() const noexcept
{
	return field_in_tesla_;
}

double MagneticField::inGauss() const noexcept
{
	return field_in_tesla_ * MagneticField::tesla_to_gauss();
}

constexpr double MagneticField::tesla_to_gauss() noexcept
{
	// TODO converting factor
	return 1.0E4;
}

} // namespace ion_calculator
