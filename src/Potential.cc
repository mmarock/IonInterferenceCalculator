

#include "Potential.hh"

namespace ion_calculator {

Potential::Potential(const double &in_volt) : value_in_volt_(in_volt) {}

Potential Potential::fromKVolt(const double &in_kvolt) {
	return Potential(in_kvolt * to_kv());
}

Potential Potential::fromVolt(const double &in_volt) {
	return Potential(in_volt);
}

double Potential::inKVolt() const noexcept {
	return value_in_volt_ / to_kv();
}

const double &Potential::inVolt() const noexcept {
	return value_in_volt_;
}

constexpr double Potential::to_kv() noexcept {
	return 1.0e3;
}

constexpr double Potential::to_mv() noexcept {
	return 1.0e6;
}

} // namespace ion_calculator
