

#ifndef ELEMENT_HH_INCLUDED
#define ELEMENT_HH_INCLUDED

#include <string>
#include <vector>

#include "Mass.hh"

namespace ion_calculator
{

/**
 * @brief One element
 *
 * Every Isotope is an element, the stripped charge
 * is not saved inside this class because I thought
 * it makes more sense after the Selector did some
 * work.
 */
class Element
{
	unsigned int atom_number_;
	unsigned int neutron_number_;
	double isotopic_composition_;
	Mass atomic_mass_;
	std::string symbol_;

public:
	Element() = delete;

	/**
	 * @brief constructor
	 *
	 * The default constructor is deleted, I have
	 * to ensure that the data is complete
	 * and a symbol is loaded (I do not care
	 * if it is the right one ;P)
	 *
	 * @param atom_number equals Z
	 * @param symbol the Elements symbol
	 * @param isotopic_composition_ composition in percent [0;1]
	 * @param neutron_number equals N
	 * @param atomic_mass I get this one in amu, I use the Mass object
	 * 	so that I can use SI units in the calculation but can input
	 * 	every other unit I want (and always know what I'm using)
	 */
	Element(unsigned int atom_number, const std::string &symbol,
		double isotopic_composition_, unsigned int neutron_number,
		const Mass &atomic_mass);

	unsigned int atomNumber() const noexcept;
	unsigned int neutronNumber() const noexcept;
	const double &isotopicComposition() const noexcept;
	const Mass &mass() const noexcept;

	/**
	 * @brief
	 *
	 * Seems strange, but I store the symbols in a seperate map.
	 * I heavily trust the small string optimisation here and
	 * this might be very stupid.
	 *
	 * @todo remove the map, instead use fixed mem or a string in place
	 *
	 * @return c string
	 */
	const char *symbol() const noexcept;

private:

};

} // ion_calculator

#endif // ELEMENT_HH_INCLUDED
