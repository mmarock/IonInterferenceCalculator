

#ifndef OUTPUT_HH_INCLUDED
#define OUTPUT_HH_INCLUDED

#include <cstdint>
#include <exception>

#include <ostream>

namespace ion_calculator
{

class Database;
class Element;
struct IonizedElement;
class MassTable;
class Selector;

/** 
 * @brief format objects nicely for the commandline
 */
class Output
{
	std::ostream &os_;
public:
	explicit Output(std::ostream &os);

	void print(const Database &db);
	void print(const Element &element);
	void print(const IonizedElement &element);
	void print(const Selector &selector);
	void error(const std::exception &ex);

};

} // namespace ion_calculator

#endif // OUTPUT_HH_INCLUDED
