

#include "Length.hh"
#include "TestSuite.hpp"

#include <cmath>

#include <iostream>

using namespace test;
using namespace ion_calculator;

using TestCase = TestSuite<Length>::TestCase;

class InMeter : public TestCase
{
	double init_val_;

public:
	explicit InMeter(const double &init_val)
	    : TestCase("InMeter"), init_val_(init_val)
	{
	}

	~InMeter() {}

	bool exec(std::ostream &os, const Length &length) const override
	{
		return std::fabs(init_val_ - length.inMeter()) < 0.001;
	}
};

int main()
{
	double val = (double)rand() / 2;
	Length l = Length::fromMeter(val);

	TestSuite<Length> suite(std::move(l));

	suite.add(new InMeter(val));

	return suite.exec(std::cout);
}
