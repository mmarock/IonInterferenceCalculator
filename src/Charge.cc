

#include "Charge.hh"

#include <iostream>

namespace ion_calculator
{

Charge::Charge() {}

Charge::Charge(int integral_value) : integral_value_(integral_value) {}

Charge Charge::fromElectronCharge(int int_value) noexcept
{
	return Charge(int_value);
}

Charge &Charge::operator+=(int inc)
{
	integral_value_ += inc;
	return *this;
}

Charge Charge::operator+(int inc) const
{
	Charge c(*this);
	c.integral_value_ += inc;
	return c;
}

int Charge::inElectronCharge() const noexcept { return integral_value_; }

double Charge::inColoumb() const noexcept
{
	return Charge::electron_charge() * integral_value_;
}

constexpr double Charge::electron_charge() noexcept { return 1.602e-19; }

bool operator<=(const Charge &lhs, const Charge &rhs) noexcept
{
	return lhs.inElectronCharge() <= rhs.inElectronCharge();
}

bool operator>(const Charge &lhs, const Charge &rhs) noexcept
{
	return lhs.inElectronCharge() > rhs.inElectronCharge();
}

} // namespace ion_calculator
