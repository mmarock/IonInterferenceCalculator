

#ifndef MASSTABLE_HH_INCLUDED
#define MASSTABLE_HH_INCLUDED

#include <vector>

#include "Charge.hh"
#include "Length.hh"
#include "MagneticField.hh"
#include "Mass.hh"
#include "Potential.hh"

namespace ion_calculator
{

class MassTable
{
public:
	// starts at q = 1
	struct Data {
		Charge charge;
		Mass mass;
	};

private:
	std::vector<Data> mass_table_;

	MagneticField field_;
	Length radius_;
	Potential terminal_voltage_;
	Charge max_;

public:
	MassTable(const MagneticField &field, const Length &radius,
		  const Potential &terminal_voltage, const Charge &max);

	const MagneticField &magneticField() const noexcept;
	const Length &radius() const noexcept;
	const Potential &terminalVoltage() const noexcept;
	const Charge &maxCharge() const noexcept;

	void set(const MagneticField &field);
	void set(const Length &radius);
	void set(const Potential &terminal);
	void set(const Charge &max);

	const std::vector<Data> &data() const noexcept;

private:

	//static constexpr double amu() noexcept;

	void recalc();

	static std::vector<MassTable::Data>
	init_table(const MagneticField &field, const Length &radius,
		   const Potential &terminal_voltage, const Charge &max);
};

} // namespace ion_calculator

#endif // MASSTABLE_HH_INCLUDED
