

#include "Length.hh"

namespace ion_calculator {

Length::Length(const double &in_m) : in_m_(in_m) {}

Length Length::fromMeter(const double &meter) noexcept {
	return Length(meter);
}

const double &Length::inMeter() const noexcept {
	return in_m_;
}

} // namespace ion_calculator
