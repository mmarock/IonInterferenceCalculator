

#ifndef ENERGY_HH_INCLUDED
#define ENERGY_HH_INCLUDED

namespace ion_calculator {

class Charge;
class Potential;

class Energy {

	double val_in_ev_;

	explicit Energy();
	explicit Energy(const double &ev);

	friend Energy operator+(const Energy &lhs, const Energy &rhs);
public:

	static Energy fromMeV(const double &mev);
	static Energy fromEv(const double &ev);

	double inMev() const noexcept;

}; // class Energy

Energy operator+(const Energy &lhs, const Energy &rhs);
Energy operator*(const Charge &charge, const Potential &potential);

} // namespace ion_calculator

#endif // ENERGY_HH_INCLUDED
