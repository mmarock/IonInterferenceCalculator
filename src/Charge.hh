

#ifndef CHARGE_HH_INCLUDED
#define CHARGE_HH_INCLUDED

namespace ion_calculator {

class Charge {
	int integral_value_;

	explicit Charge();
	explicit Charge(int integral_value);
public:
	static Charge fromElectronCharge(int int_value) noexcept;

	Charge &operator+=(int inc);
	Charge operator+(int inc) const;

	int inElectronCharge() const noexcept;
	double inColoumb() const noexcept;

private:
	static constexpr double electron_charge() noexcept;

}; // class Charge

bool operator<=(const Charge &lhs, const Charge &rhs) noexcept;
bool operator>(const Charge &lhs, const Charge &rhs) noexcept;

} // namespace ion_calculator

#endif // CHARGE_HH_INCLUDED
