

#ifndef MASS_HH_INCLUDED
#define MASS_HH_INCLUDED

namespace ion_calculator
{

class Mass
{
	double amu_;

	explicit Mass(const double &value);

	friend double operator/(const Mass &lhs, const Mass &rhs);
	friend Mass operator-(const Mass &lhs, const Mass &rhs);
	friend Mass operator+(const Mass &lhs, const Mass &rhs);
	friend bool operator<(const Mass &lhs, const Mass &rhs);
	friend bool operator>(const Mass &lhs, const Mass &rhs);

public:
	explicit Mass();

	static Mass fromKg(const double &kg);
	static Mass fromAmu(const double &amu);
	static Mass fromMevCSquared(const double &mev_c_squared);

	Mass(const Mass &rhs) = default;
	Mass &operator=(const Mass &rhs) = default;

	double inKg() const noexcept;
	const double &inAmu() const noexcept;
	double inMevCSquared() const noexcept;

private:
	static constexpr double amu() noexcept;
	static constexpr double mev_c_squared() noexcept;
};

double operator/(const Mass &lhs, const Mass &rhs);
Mass operator-(const Mass &lhs, const Mass &rhs);
Mass operator+(const Mass &lhs, const Mass &rhs);
bool operator<(const Mass &lhs, const Mass &rhs);
bool operator>(const Mass &lhs, const Mass &rhs);

} // namespace ion_calculator

#endif // MASS_HH_INCLUDED
