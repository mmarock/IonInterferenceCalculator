

#ifndef POTENTIAL_HH_INCLUDED
#define POTENTIAL_HH_INCLUDED

namespace ion_calculator {

class Potential {
	double value_in_volt_;

	explicit Potential(const double &in_volt);
public:
	static Potential fromKVolt(const double &in_kvolt);
	static Potential fromVolt(const double &in_volt);

	double inKVolt() const noexcept;
	const double &inVolt() const noexcept;

private:
	static constexpr double to_kv() noexcept;
	static constexpr double to_mv() noexcept;
};

} // namespace ion_calculator

#endif // POTENTIAL_HH_INCLUDED
