

#ifndef LENGTH_HH_INCLUDED
#define LENGTH_HH_INCLUDED

namespace ion_calculator {

class Length {
	double in_m_;

	explicit Length(const double &in_m);
public:
	static Length fromMeter(const double &meter) noexcept;
	
	const double &inMeter() const noexcept;
};


} // namespace ion_calculator

#endif // LENGTH_HH_INCLUDED
