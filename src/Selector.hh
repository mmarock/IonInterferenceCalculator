

#ifndef SELECTOR_HH_INCLUDED
#define SELECTOR_HH_INCLUDED

#include "Charge.hh"
#include "Database.hh"
#include "Element.hh"
#include "Energy.hh"
#include "MassTable.hh"

namespace ion_calculator
{

class Length;
class MagneticField;
class Potential;

struct IonizedElement {
	Element element;
	Charge charge;
	double mass_overlap;
	Energy energy;
};

class Selector
{
	MassTable mtable_;
	Mass deviation_;
	Potential injection_;

public:
	Selector(const MagneticField &field, const Mass &mass_deviation,
		 const Length &radius, const Potential &terminal,
		 const Potential &injection, const Charge &max);

	const MagneticField &magneticField() const noexcept;
	const Mass &massDeviation() const noexcept;
	const Length &radius() const noexcept;
	const Potential &terminalVoltage() const noexcept;
	const Charge &maxCharge() const noexcept;

	void set(const MagneticField &field);
	void set(const Mass &mass_deviation);
	void set(const Length &radius);
	void set(const Potential &terminal);
	void set(const Charge &max);

	const MassTable &massTable() const noexcept;

	using ResultVector = std::vector<IonizedElement>;
	void query(const Element &e, ResultVector &v) const noexcept;
};

} // namespace ion_calculator

#endif // SELECTOR_HH_INCLUDED
